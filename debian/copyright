Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: blackbox
Source: https://sourceforge.net/projects/blackboxwm/

Files: *
Copyright:
           1997-2005 Bradley T Hughes <bhughes at trolltech.com>
           2000      Ales Kosir <ales.kosir@hermes.si>
           2000      Bertrand Duret <bertrand.duret@libertysurf.fr>
           2000-2002 Timothy M. King <tmk@lordzork.com>
           2000-2002 Wilbert Berendsen <wbsoft@xs4all.nl>
           2001-2005 Sean 'Shaleh' Perry <shaleh at debian.org>
           2002      R.B. "Brig" Young II <secretsaregood@yahoo.com>
           2002      Satoh Satoru <ss@gnome.gr.jp>
License: Expat

Files: debian/*
Copyright: 2000      Brent A. Fulgham <bfulgham@debian.org>
           2000-2002 Sean 'Shaleh' Perry <shaleh@debian.org>
           2001      LaMont Jones <lamont@smallone.fc.hp.com>
           2003      David Pashley <david@davidpashley.com>
           2003      Jesus Climent <jesus.climent@hispalinux.es>
           2004-2007 Bruno Barrera C. <bruno@debian.org>
           2006      Luk Claes <luk@debian.org>
           2008      Marc 'HE' Brockschmidt <he@debian.org>
           2008      Simon McVittie <smcv@ianadd.pseudorandom.co.uk>
           2011      Jakub Wilk <jwilk@debian.org>
           2011-2014 HIGUCHI Daisuke (VDR dai) <dai@debian.org>
           2012      David Prevot <taffit@debian.org>
           2012      Matthias Klose <doko@debian.org>
           2014-2016 Herbert Parentes Fortes Neto <hpfn@ig.com.br>
           2015-2020 Joao Eriberto Mota Filho <eriberto@debian.org>
           2015      James Cowgill <james410@cowgill.org.uk>
           2015      Sebastian Ramacher <sramacher@debian.org>
           2015      Steve Langasek <vorlon@debian.org>
           2017      Simon Quigley <tsimonq2@ubuntu.com>
           2018      Adrian Bunk <bunk@debian.org>
           2019      Boyuan Yang <byang@debian.org>
License: GPL-2+

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
